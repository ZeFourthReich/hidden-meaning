package com.litimran.hiddenmeaning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class DifficultySelect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty_select);
    }

    public void Difficulties2Start(View view){
        Intent intent = new Intent(this, StartScreen.class);
        startActivity(intent);
    }

    public void Difficulties2Easy(View view){
        Intent intent = new Intent(this, EasyLvlSelect.class);
        startActivity(intent);
    }

    public void Difficulties2Med(View view){
        Intent intent = new Intent(this, MedLvlSelect.class);
        startActivity(intent);
    }

    public void Difficulties2Hard(View view){
        Intent intent = new Intent(this, HardLvlSelect.class);
        startActivity(intent);
    }

    public void Difficulties2Adv(View view){
        Intent intent = new Intent(this, AdvLvlSelect.class);
        startActivity(intent);
    }
}
