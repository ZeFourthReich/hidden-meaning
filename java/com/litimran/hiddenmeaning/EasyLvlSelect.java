package com.litimran.hiddenmeaning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class EasyLvlSelect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_lvl_select);
    }

    public void Ez2Difficulties(View view){
        Intent intent = new Intent(this, DifficultySelect.class);
        startActivity(intent);
    }

    public void Ez2One(View view){
        Intent intent = new Intent(this, Ez1.class);
        startActivity(intent);
    }
}
