package com.litimran.hiddenmeaning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;


public class Ez1 extends AppCompatActivity {

    private int ez1a1switch = 0;
    private int jitbslots = 0;
    private char[] guess = new char[10];
    private char[] answer = {'j','a','c','k','i','n','a','b','o','x'};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ez1);
    }

    public void OneTOEz(View view){
        Intent intent = new Intent(this, EasyLvlSelect.class);
        startActivity(intent);
    }

    public void Ez1A1(View view){
        jitbslots = 1;
        Button ezONEaONE = (Button) findViewById(R.id.ez1a1);
        if(ez1a1switch == 0 && jitbslots < 10){
            if(jitbslots == 0){
                RelativeLayout.LayoutParams OnePos = (RelativeLayout.LayoutParams) ezONEaONE.getLayoutParams();
                OnePos.bottomMargin = 515;
                ezONEaONE.setLayoutParams(OnePos);
                guess[0] = 'a';
            }else if(jitbslots == 1){
                RelativeLayout.LayoutParams TwoPos = (RelativeLayout.LayoutParams) ezONEaONE.getLayoutParams();
                TwoPos.bottomMargin = 515;
                ezONEaONE.setLayoutParams(TwoPos);
                guess[1] = 'a';
            }
            jitbslots++;
            ez1a1switch = 1;
        }else if(ez1a1switch == 1){
            RelativeLayout.LayoutParams ez1a1BACK = (RelativeLayout.LayoutParams) ezONEaONE.getLayoutParams();
            ez1a1BACK.setMargins(0,0,0,35);
            ezONEaONE.setLayoutParams(ez1a1BACK);
            jitbslots--;
            ez1a1switch = 0;
        }
    }
}
