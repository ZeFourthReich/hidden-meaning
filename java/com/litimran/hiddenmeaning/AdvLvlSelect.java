package com.litimran.hiddenmeaning;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AdvLvlSelect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adv_lvl_select);
    }
    public void Adv2Difficulties(View view){
        Intent intent = new Intent(this, DifficultySelect.class);
        startActivity(intent);
    }
}
